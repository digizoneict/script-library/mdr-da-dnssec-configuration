<?php

/**
 * DNSSEC Activation Script.
 *
 * Script to activate DNSSEC in DirectAdmin and add the keys to MijnDomeinReseller.
 * This script is only for domains registered at MijnDomein Reseller and hosted in DirectAdmin.
 * 
 * @author Jeroen Spaans <jeroen@tomorrowsit.nl>
 * @package dnssec_activation
 * @version 1.1
 *
 * 1.1
 * - Update httpsocket to version 3.0.4
 * - Make the script more flexible in usage of algorithm
 * - Add some extra logging of parameters
 *
 * 1.0
 * Initial Release of the script
 *
 */

# Include required files
include 'lib/da/httpsocket.php';
include 'lib/mdr/class_MDR_api.php';
require __DIR__ . '/vendor/autoload.php';

# Include config file
include 'config/config.php';

# Load logger
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

$log = new Logger('dnssec');
$log->pushHandler(new StreamHandler('log/dnssec.log.'.date('YmdHis'), Logger::INFO));
$log->info('Start DNSSEC Activation Script');

## Get configuration parameters
# MDR Parameters
$mdr_username = $conf_mdr_username;
$mdr_password = $conf_mdr_password;
$mdr_authtype = $conf_mdr_auth;
$mdr_ns_id = $conf_mdr_ns_id;

# DirectAdmin Parameters
$da_url = $conf_da_url;
$da_port = $conf_da_port;
$da_username = $conf_da_username;
$da_password = $conf_da_password;

# Set empty params
$mdr_domains = array();
$mdr_dnssec_domains = array();

# Step 1: Get domain list from MijnDomeinReseller

$log->info('Step 1: Get domain list from MijnDomeinReseller');

$mdr = new MDR_api();

$mdr->AddParam( "user" , $mdr_username );
$mdr->AddParam( "pass" , $mdr_password );
$mdr->AddParam( "authtype" , $mdr_authtype );

$mdr->addParam( "command", "domain_list" );
$mdr->addParam( "sort", "domein" );
$mdr->addParam( "order", "0" );

$mdr->DoTransaction();


if( $mdr->Values[ "errcount" ] > 0 )
{
    for( $i=1;$i<=$mdr->Values[ "errcount" ];$i++ ) {
        $log->error($mdr->Values[ "errnotxt".$i ] . " (code: " . $mdr->Values[ "errno".$i ] . ")");
    }
}
else {
    for($i=0;$i<$mdr->Values["domeincount"];$i++) {
        if ($mdr->Values["status[$i]"] == "active")
        {
            $mdr_domains[] = $mdr->Values["domein[$i]"];
        }
    }

    $log->info('Total domains found at MDR: ' . $i);
}

# Step 2: Remove domains from array that are using MDR DNS Servers

$log->info('Step 2: Remove domains from array that are using MDR DNS Servers or using other NameServers');

foreach ($mdr_domains as $mdr_domain)
{
    $fdomain = explode('.', $mdr_domain);
    $dname = $fdomain[0];
    $dtld = $fdomain[1];

    $mdr = new MDR_API();

    $mdr->AddParam( "user" , $mdr_username );
    $mdr->AddParam( "pass" , $mdr_password );
    $mdr->AddParam( "authtype" , $mdr_authtype );

    $mdr->addParam( "command", "domain_get_details" );
    $mdr->addParam( "domein",  $dname );
    $mdr->addParam( "tld",     $dtld );

    $mdr->DoTransaction();

    if( $mdr->Values[ "errcount" ] > 0 ) {
        for( $i=1;$i<=$mdr->Values[ "errcount" ];$i++ ) {
            $log->error($mdr->Values[ "errnotxt".$i ] . " (code: " . $mdr->Values[ "errno".$i ] . ")");
        }

    } else {

        if ($mdr->Values['gebruik_dns'] == 1)
        {
            $log->info($mdr_domain . ': skipped, Nameservers MDR are used');
            $drmkey = array_search("$mdr_domain", $mdr_domains);
            unset($mdr_domains[$drmkey]);
        }
        elseif ($mdr->Values['ns_id'] != $mdr_ns_id)
        {
            $log->info($mdr_domain . ': skipped, Other nameservers are used');
            $drmkey = array_search("$mdr_domain", $mdr_domains);
            unset($mdr_domains[$drmkey]);
        }
    }
}

# Step 3: Check if DNSSEC is activated for all domains at MijnDomeinReseller

$log->info('Step 3: Check if DNSSEC is activated for all domains at MijnDomeinReseller');

foreach ($mdr_domains as $mdr_domain)
{
    $fdomain = explode('.', $mdr_domain);
    $dname = $fdomain[0];
    $dtld = $fdomain[1];

    $mdr = new MDR_API();

    $mdr->AddParam( "user" , $mdr_username );
    $mdr->AddParam( "pass" , $mdr_password );
    $mdr->AddParam( "authtype" , $mdr_authtype );

    $mdr->addParam( "command", "dnssec_get_details"  );
    $mdr->addParam( "domein",  $dname );
    $mdr->addParam( "tld",     $dtld );

    $mdr->DoTransaction();

    if( $mdr->Values[ "errcount" ] > 0 ) {
         for( $i=1;$i<=$mdr->Values[ "errcount" ];$i++ ) {
            $log->error($mdr->Values[ "errnotxt".$i ] . " (code: " . $mdr->Values[ "errno".$i ] . ")");
        }

    } else {

        if ($mdr->Values['recordcount'] == 0)
        {
            $log->info($mdr_domain . ': Currently no DNSSEC');
            $mdr_dnssec_domains[] = $mdr_domain;
        }
    }
}

# Step 4: Check domain in DirectAdmin, Generate keys & Sign Zone OR Push keys to MijnDomeinReseller

$log->info('Step 4: Check domain in DirectAdmin, Generate keys & Sign Zone OR Push keys to MijnDomeinReseller');

if (!empty($mdr_dnssec_domains))
{
    # Setup DirectAdmin connection
    $DA = new HTTPSocket();
    $DA->connect($da_url, $da_port);
    $DA->set_login($da_username, $da_password);

    foreach ($mdr_dnssec_domains as $mdr_dnssec_domain)
    {

        $DA->set_method('GET');
        $DA->query('/CMD_API_DNS_ADMIN',
            array(
               'action' => 'exists',
               'domain' => $mdr_dnssec_domain
            ));
        $result = $DA->fetch_body();
        parse_str($result, $output);

        if (isset($output['exists']) && $output['exists'] == 1) {
            $DA->set_method('GET');
            $DA->query('/CMD_API_DNS_ADMIN',
                array(
                    'action' => 'dnssec',
                    'domain' => $mdr_dnssec_domain,
                    'value' => 'get_keys'
                ));
            $result = $DA->fetch_body();
            parse_str($result, $output);

            if (isset($output['ksk_DNSKEY'])) {

                $log->info($mdr_dnssec_domain . ': KSK exists');

                if (isset($output['signed_on'])) {

                    $log->info($mdr_dnssec_domain . ': Zone already signed');

                    $fdomain = explode('.', $mdr_dnssec_domain);
                    $dname = $fdomain[0];
                    $dtld = $fdomain[1];

                    $log->debug('KSK from DA: ' . $output['ksk_DNSKEY']);
                    $fksk = explode(' ', $output['ksk_DNSKEY'], 4);

                    $flag = $fksk[0];
                    $log->debug('Flag: ' . $flag);
                    
                    $alg = $fksk[2];
                    $log->debug('Alg: ' . $alg);
                    
                    $ksk = $fksk[3];
                    $log->debug('KSK: ' . $ksk);

                    $mdr = new MDR_api();

                    $mdr->AddParam("user", $mdr_username);
                    $mdr->AddParam("pass", $mdr_password);
                    $mdr->AddParam("authtype", $mdr_authtype);

                    $mdr->addParam("command", "dnssec_record_add");
                    $mdr->addParam("domein", $dname);
                    $mdr->addParam("tld", $dtld);

                    $mdr->addParam("flag", $flag);
                    $mdr->addParam("algorithm", $alg);
                    $mdr->addParam("publickey", $ksk);

                    $mdr->DoTransaction();

                    if ($mdr->Values["errcount"] > 0) {
                        print "Fout opgetreden:<BR>";

                        for ($i = 1; $i <= $mdr->Values["errcount"]; $i++) {
                            $log->error($mdr->Values[ "errnotxt".$i ] . " (code: " . $mdr->Values[ "errno".$i ] . ")");
                        }

                    } else {
                        $log->info($mdr_dnssec_domain . ': KSK added at MDR');
                    }

                } else {

                    $log->info($mdr_dnssec_domain . ': Zone not signed, signing now');

                    # Sign zone
                    $DA->set_method('POST');
                    $DA->query('/CMD_API_DNS_ADMIN',
                        array(
                            'action' => 'dnssec',
                            'domain' => $mdr_dnssec_domain,
                            'sign_zone' => 'anything',
                        ));
                    $result = $DA->fetch_body();

                    $log->info($mdr_dnssec_domain . ': Zone signed');
                }
            } else {
                # Generate DNSSec Keys for domain

                $log->info($mdr_dnssec_domain . ': Generate DNSSEC keys');

                $DA->set_method('POST');
                $DA->query('/CMD_API_DNS_ADMIN',
                    array(
                        'action' => 'dnssec',
                        'generate_keys' => 'anything',
                        'domain' => $mdr_dnssec_domain,
                    ));
                $result = $DA->fetch_body();

                $log->info($mdr_dnssec_domain . ': Sign zone');

                # Sign zone
                $DA->set_method('POST');
                $DA->query('/CMD_API_DNS_ADMIN',
                    array(
                        'action' => 'dnssec',
                        'domain' => $mdr_dnssec_domain,
                        'sign_zone' => 'anything',
                    ));
                $result = $DA->fetch_body();

                $log->info($mdr_dnssec_domain . ': Keys generated and zone signed');
            }
        }
        else
        {
            $log->warning($mdr_dnssec_domain . ': Not exists in DirectAdmin');
        }
    }
}
else
{
    $log->info('Skipped: No domains withoud DNSSEC');
}

$log->info('Script finished');
