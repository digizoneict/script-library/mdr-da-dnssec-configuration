# DNSSEC activation script

Het DNSSEC activation script zorgt ervoor dat domeinen die zowel bij MijnDomeinReseller (MDR) staan als in DirectAdmin worden voorzien van DNSSEC.

## Stappen in het script

* Opvragen domeinnamen zonder DNSSEC in MijnDomeinReseller middels API
* Controleren of domeinnamen in DirectAdmin al DNSSEC keys hebben en de zone als gesigned is
* Indien geen keys: Keys genereren en zone signen
* Indien keys, maar niet gesigned: Keys opnieuw genereren en zone signen
* Indien keys & gesigned: DNSSEC keys toevoegen in MDR

Tussen de stap keys generen/signen en de stap DNSSEC keys toevoegen in MDR dient 24 uur te zitten. Daarom dient het script maximaal 1 keer per 24 uur gedraaid te worden en duurt het maximaal dus 48 uur alvorens DNSSEC voor een domein is geactiveerd.

## Installeren & Configureren

Het script kan in principe op elke server/client met daarop PHP worden geïnstalleerd, aanbeveling is om het script te plaatsen op de directadmin server.

### Aanmaken Login Key for DirectAdmin user with specific API permissions

Zorg dat de login keys feature is enabled in DirectAdmin:

```bash
vi /usr/local/directadmin/conf/directadmin.conf
```

Add

```dotenv
login_keys=1
```

And in the user.conf file

```bash
/usr/local/directadmin/data/users/admin/user.conf
```

Add

```dotenv
login_keys=ON
```

Herstart DirectAdmin

```bash
systemctl restart directadmin
```

Ga naar het DirectAdmin portal en switch to the user section of the admin user

* Klik op Login keys
* Maak een key aan met enkel de rechten voor CMD_API_DNS_ADMIN en enkel toegestaan vanaf het ip-adres waar je het script draaid.

### Instellen MijnDomeinReseller

Ga naar de MDR portal en login

* Ga naar Account -> Instellingen -> API
* Vink API inschakelen aan
* Vul bij API ipadres het ip-adres in vanaf waar je het script draait

Het wachtwoord willen we niet plaintext in de configuratie hebben staan, daarom moet deze eerst voorzien worden van een hash.
Voer daarvoor het volgende commando uit:

```bash
php -r "md5('jewachtwoordhier');"
``` 

Vul bovenstaande hash in bij de parameter $conf_mdr_password.

### Installeren script

#### Requirements
* git
* php > 7.2
* composer

### Installeren

```bash
mkdir /beheer/scripts
cd /beheer/scripts
git clone git@gitlab.com:digizoneict/script-library/mdr-da-dnssec-configuration.git
cd mdr-da-dnssec-configuration
composer install
```

### Configureren

```bash
cp config/config.php.example config.php
vi config.php
```

Vul de gevraagde configuratie parameters in.

### Aanmaken cron

```bash
crontab -e
```

Voeg de onderstaande regel toe, met deze cron wordt het script elke dag om 23:00 uur uitgevoerd.

```cron
0 23 * * * cd /beheer/scripts/mdr-da-dnssec-configuration && /usr/bin/php dnssec_activation.php >/dev/null 2>&1
```